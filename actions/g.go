package actions

import (
    "net/http"
    
	"github.com/gobuffalo/buffalo"
)

// GMg default implementation.
func GMg(c buffalo.Context) error {
	return c.Render(http.StatusOK, r.HTML("g/mg.html"))
}

