package actions

import (
    "net/http"
    
	"github.com/gobuffalo/buffalo"
)

// LinuxShowlinux default implementation.
func LinuxShowlinux(c buffalo.Context) error {
	return c.Render(http.StatusOK, r.HTML("linux/showlinux.html"))
}

