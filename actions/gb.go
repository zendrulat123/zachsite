package actions

import (
    "net/http"
    
	"github.com/gobuffalo/buffalo"
)

// GbMgb default implementation.
func GbMgb(c buffalo.Context) error {
	return c.Render(http.StatusOK, r.HTML("gb/mgb.html"))
}

