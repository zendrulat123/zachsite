package actions

import (
    "net/http"
    
	"github.com/gobuffalo/buffalo"
)

// AMa default implementation.
func AMa(c buffalo.Context) error {
	return c.Render(http.StatusOK, r.HTML("a/ma.html"))
}

