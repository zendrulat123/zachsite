package actions

import (
    "net/http"
    
	"github.com/gobuffalo/buffalo"
)

// RMr default implementation.
func RMr(c buffalo.Context) error {
	return c.Render(http.StatusOK, r.HTML("r/mr.html"))
}

