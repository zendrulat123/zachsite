package actions

import (
	"net/http"

	"github.com/gobuffalo/buffalo"
)

// var r *render.Engine
// var assetsBox = packr.New("app:assets", "../public")

// AMa default implementation.
func Thoughtone(c buffalo.Context) error {
	// r = render.New(render.Options{
	// 	// HTML layout to be used for all HTML requests:
	// 	HTMLLayout: "application.plush.html",

	// 	// Box containing all of the templates:
	// 	TemplatesBox: packr.New("app:templates", "../templates"),
	// 	AssetsBox:    assetsBox,
	// })
	return c.Render(http.StatusOK, r.HTML("thoughts/thoughtone/thoughtone.html"))
}
