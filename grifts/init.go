package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/zendrulat123/zachsite/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
